# Информация о проекте

jse-14

## Задача реализует:

#### - команды, не требующие авторизации:
```bash
    "help"       - список команд;
    "version"    - версия приложения;
    "about"      - информация о разработчике;
    "info"       - информация о системе;
    "arguments"  - перечень аргументов;
    "commands"   - перечен команд, не требующих аутентификации;
    "exit"       - выход из приложения;
```
#### - CRUD команды, доступные после авторизации пользователя:
```bash
    "task-create"            - создать задачу; 
    "task-list"              - показать список задач;
    "task-clear"             - удалить существующие задачи;
    "task-update-by-id"      - обновить задачу по id;
    "task-update_by_index"   - обновить задачу по index;
    "task-view-by-id"        - показать задачу по id;
    "task-view-by-index"     - показать задачу по index;
    "task-view-by-name"      - показать задачу по name;
    "task-remove-by-id"      - удалить задачу по id;
    "task-remove-by-index"   - удалить задачу по index;
    "task-remove-by-name"    - удалить задачу по name;

    "project-create"          - создать проект;
    "project-list"            - показать список проектов;
    "project-clear"           - удалить существующие проекты;
    "project-update-by-id"    - обновить проект по id;
    "project-update_by_index" - обновить проект по inde;
    "project-view-by-id"      - показать проект по id;
    "project-view-by-index"   - показать проект по index;
    "project-view-by-name"    - показать проект по name;
    "project-remove-by-id"    - удалить проект по id;
    "project-remove-by-index" - удалить проект по index;
    "project-remove-by-name"  - удалить проект по name;
```
#### - команды авторизации и управления учетной записью:
```bash
    "login"                   - авторизация в сеансе;
    "logout"                  - выход из сеанса;
    "registry"                - регистрауия нового пользователя;
    "list-users"              - список зарегистрированных пользователей 
                                (доступно только пользователю "admin");
    "change-password"         - изменение пароля пользователя сеанса;
    "profiler-of-user"        - вывод информации о пользователе сеанса (пользователь "admin" 
                                может просматривать информацию о других пользователях);
    "change-profiler-of-user" - изменение данных пользователя сеанса;
```
#### - тестовые пользователи:
```bash
    login: "admin", password: "admin"
    login: "test",  password: "test"
```
## Стек технологий

java/Intellij IDEA/Apache Maven/Git

## Требования к SOFTWARE

- JDK 1.8

## Команда для сборки проекта

```bash
mvn clean package
```

## Команда для запуска проекта

```bash
java -jar ./target/task-manager.jar
```

## Информация о разработчике

**ФИО**: Баулина Ольга Александровна

**E-MAIL**: golovolomkacom@gmail.com

package ru.baulina.tm.controller;

import ru.baulina.tm.Entity.User;
import ru.baulina.tm.api.controller.IAuthController;
import ru.baulina.tm.api.service.IAuthService;
import ru.baulina.tm.util.TerminalUtil;

import java.util.List;

public class AuthController implements IAuthController {

    private final IAuthService authService;

    public AuthController(final IAuthService authService) {
        this.authService = authService;
    }

    @Override
    public void login() {
        System.out.println("[LOGIN]");
        System.out.println("ENTER LOGIN:");
        final String login = TerminalUtil.nextLine();
        System.out.println("ENTER PASSWORD:");
        final String password = TerminalUtil.nextLine();
        authService.login(login, password);
        System.out.println("[OK]");
    }

    @Override
    public void logout() {
        System.out.println("[LOGOUT]");
        authService.logout();
        System.out.println("[OK]");
    }

    @Override
    public void registry() {
        System.out.println("[REGISTRY]");
        System.out.println("ENTER LOGIN:");
        final String login = TerminalUtil.nextLine();
        System.out.println("ENTER PASSWORD:");
        final String password = TerminalUtil.nextLine();
        System.out.println("ENTER E-MAIL:");
        final String email = TerminalUtil.nextLine();
        authService.registry(login, password, email);
        System.out.println("[OK]");
    }

    @Override
    public void changePassword() {
        authService.isAuth();
        System.out.println("[CHANGE_PASSWORD]");
        System.out.println("ENTER OLD PASSWORD:");
        final String passwordOld = TerminalUtil.nextLine();
        System.out.println("ENTER NEW PASSWORD:");
        final String passwordNew = TerminalUtil.nextLine();
        authService.changePassword(passwordOld, passwordNew);
        System.out.println("[OK]");
    }

    @Override
    public void profileUser() {
        authService.isAuth();
        System.out.println("[SHOW_PROFILE_OF_USER]");
        System.out.println("ENTER LOGIN:");
        final String login = TerminalUtil.nextLine();
        final User user = authService.findByLogin(login);
        System.out.println("LOGIN: " + user.getLogin());
        System.out.println("PASSWORD: " + user.getPasswordHash());
        System.out.println("E-MAIL: " + user.getEmail());
        System.out.println("FEST NAME: " + user.getFestName());
        System.out.println("LAST NAME: " + user.getLastName());
        System.out.println("[OK]");
    }

    @Override
    public void showListUser() {
        authService.isAuth();
        final List<User> users = authService.findAll();
        System.out.println("[SHOW_LIST_USERS]");
        int index = 1;
        for (User user: users) {
            System.out.println(index + ". ");
            System.out.println("LOGIN: " + user.getLogin());
            System.out.println("PASSWORD: " + user.getPasswordHash());
            System.out.println("E-MAIL: " + user.getEmail());
            System.out.println("FEST NAME: " + user.getFestName());
            System.out.println("LAST NAME: " + user.getLastName());
            index++;
        }
        System.out.println("[OK]");
    }

    @Override
    public void changeProfileUser() {
        authService.isAuth();
        System.out.println("[CHANGE_PROFILE_OF_USER]");
        System.out.println("ENTER E-MAIL: ");
        final String newEmail = TerminalUtil.nextLine();
        System.out.println("ENTER FEST NAME: ");
        final String newFestName = TerminalUtil.nextLine();
        System.out.println("ENTER LAST NAME: ");
        final String newLastName = TerminalUtil.nextLine();
        authService.changeUser(newEmail, newFestName, newLastName);
        System.out.println("[OK]");
    }

}

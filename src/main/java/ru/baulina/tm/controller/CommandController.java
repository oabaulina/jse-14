package ru.baulina.tm.controller;

import ru.baulina.tm.api.controller.ICommandController;
import ru.baulina.tm.api.service.ICommandService;
import ru.baulina.tm.dto.Command;
import ru.baulina.tm.util.SystemInformation;

import java.util.Arrays;

public class CommandController implements ICommandController {

    private final ICommandService commandService;

    public CommandController(final ICommandService commandService) {
        this.commandService = commandService;
    }

    public void showHelp() {
        System.out.println("[HELP]");
        final Command[] commands = commandService.getTerminalCommands();
        for (final Command command: commands) System.out.println(command);
        System.out.println();
    }

    public void showVersion() {
        System.out.println("[VERSION]");
        System.out.println("1.0.14");
        System.out.println();
    }

    public void showAbout() {
        System.out.println("[ABOUT]");
        System.out.println("Olga Baulina");
        System.out.println("golovolomkacom@gmail.com");
        System.out.println();
    }

    public void showCommands() {
        final String[] commands = commandService.getCommands();
        System.out.println(Arrays.toString(commands));
        System.out.println();
    }

    public void showArguments() {
        final String[] arguments = commandService.getArgs();
        System.out.println(Arrays.toString(arguments));
        System.out.println();
    }

    public void showInfo() {
        System.out.println("[INFO]");
        System.out.println("Available processors (cores): " + SystemInformation.availableProcessors);
        System.out.println("Free memory: " + SystemInformation.freeMemoryFormat);
        System.out.println("Maximum memory: " + SystemInformation.maxMemoryFormat);
        System.out.println("Total memory available to JVM: " + SystemInformation.totalMemoryFormat);
        System.out.println("Used memory by JVM: " + SystemInformation.usedMemoryFormat);
        System.out.println();
    }

    public void displayWelcome() {
        System.out.println("** Welcome to task manager **");
        System.out.println();
    }

    public  void exit() {
        System.exit(0);
    }

}

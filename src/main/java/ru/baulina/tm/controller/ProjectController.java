package ru.baulina.tm.controller;

import ru.baulina.tm.api.controller.IProjectController;
import ru.baulina.tm.api.service.IAuthService;
import ru.baulina.tm.api.service.IProjectService;
import ru.baulina.tm.Entity.Project;
import ru.baulina.tm.api.service.ITaskService;
import ru.baulina.tm.util.TerminalUtil;

import java.util.List;

public class ProjectController implements IProjectController {

    private final IProjectService projectService;
    private final IAuthService authService;

    public ProjectController(final IProjectService projectService, final IAuthService authService) {
        this.projectService = projectService;
        this.authService = authService;
    }

    @Override
    public void showProjects() {
        System.out.println("[LIST PROJECT]");
        final Long userId = authService.getUserId();
        final List<Project> projects = projectService.findAll(userId);
        int index = 1;
        for (Project project: projects) {
            System.out.println(index + ". " + project);
            index++;
        }
        System.out.println("[OK]");
        System.out.println();
    }

    @Override
    public void clearProjects() {
        System.out.println("[CLEAR PROJECT]");
        final Long userId = authService.getUserId();
        projectService.clear(userId);
        System.out.println("[OK]");
        System.out.println();
    }

    @Override
    public void createProjects() {
        System.out.println("[CREATE PROJECT]");
        System.out.println("ENTER NAME:");
        final  String name = TerminalUtil.nextLine();
        System.out.println("ENTER DESCRIPTION:");
        final  String description = TerminalUtil.nextLine();
        final Long userId = authService.getUserId();
        projectService.create(userId, name, description);
        System.out.println("[OK]");
        System.out.println();
    }

    @Override
    public void showProjectById() {
        System.out.println("[SHOW PROJECT]");
        System.out.println("ENTER ID");
        final Long id = TerminalUtil.nexLong();
        final Long userId = authService.getUserId();
        final Project project = projectService.findOneById(userId, id);
        if (project == null) {
            System.out.println("[FAIL]");
            return;
        }
        showProject(project);
        System.out.println("[OK]");
    }

    @Override
    public void showProjectByIndex() {
        System.out.println("[SHOW PROJECT]");
        System.out.println("ENTER INDEX");
        final Integer index = TerminalUtil.nexInt() -1;
        final Long userId = authService.getUserId();
        final Project project = projectService.findOneByIndex(userId, index);
        if (project == null) {
            System.out.println("[FAIL]");
            return;
        }
        showProject(project);
        System.out.println("[OK]");
    }

    private  void showProject(final Project project) {
        if (project == null) return;
        System.out.println("ID: " + project.getId());
        System.out.println("NAME: " + project.getName());
        System.out.println("DESCRIPTION: " + project.getDescription());
    }

    @Override
    public void showProjectByName() {
        System.out.println("[SHOW PROJECT]");
        System.out.println("ENTER NAME");
        final String name = TerminalUtil.nextLine();
        final Long userId = authService.getUserId();
        final Project project = projectService.findOneByName(userId, name);
        if (project == null) {
            System.out.println("[FAIL]");
            return;
        }
        showProject(project);
        System.out.println("[OK]");
    }

    @Override
    public void updateProjectById() {
        System.out.println("[SHOW PROJECT]");
        System.out.println("ENTER ID:");
        final Long id  = TerminalUtil.nexLong();
        final Long userId = authService.getUserId();
        final Project project = projectService.findOneById(userId, id);
        if (project == null) {
            System.out.println("[FAIL]");
            return;
        }
        System.out.println("ENTER NAME:");
        final String name  = TerminalUtil.nextLine();
        System.out.println("ENTER DESCRIPTION:");
        final String description  = TerminalUtil.nextLine();
        final Project projectUpdated = projectService.updateTaskById(userId, id, name, description);
        if (projectUpdated == null) {
            System.out.println("[FAIL]");
            return;
        }
        System.out.println("[OK]");
    }

    @Override
    public void updateProjectByIndex() {
        System.out.println("[SHOW PROJECT]");
        System.out.println("ENTER INDEX:");
        final Integer index  = TerminalUtil.nexInt() -1;
        final Long userId = authService.getUserId();
        final Project project = projectService.findOneByIndex(userId, index);
        if (project == null) {
            System.out.println("[FAIL]");
            return;
        }
        System.out.println("ENTER NAME:");
        final String name  = TerminalUtil.nextLine();
        System.out.println("ENTER DESCRIPTION:");
        final String description  = TerminalUtil.nextLine();
        final Project projectUpdated = projectService.updateTaskByIndex(userId, index, name, description);
        if (projectUpdated == null) {
            System.out.println("[FAIL]");
            return;
        }
        System.out.println("[OK]");
    }

    @Override
    public void removeProjectById() {
        System.out.println("[SHOW PROJECT]");
        System.out.println("ENTER ID");
        final Long id = TerminalUtil.nexLong();
        final Long userId = authService.getUserId();
        final Project project = projectService.removeOneById(userId, id);
        if (project == null) System.out.println("[FAIL]");
        else System.out.println("[OK]");
    }

    @Override
    public void removeProjectByIndex() {
        System.out.println("[SHOW PROJECT]");
        System.out.println("ENTER INDEX");
        final Integer index = TerminalUtil.nexInt() -1;
        final Long userId = authService.getUserId();
        final Project project = projectService.removeOneByIndex(userId, index);
        if (project == null) System.out.println("[FAIL]");
        else System.out.println("[OK]");
    }

    @Override
    public void removeProjectByName() {
        System.out.println("[SHOW PROJECT]");
        System.out.println("ENTER NAME");
        final String name = TerminalUtil.nextLine();
        final Long userId = authService.getUserId();
        final Project project = projectService.removeOneByName(userId, name);
        if (project == null) System.out.println("[FAIL]");
        else System.out.println("[OK]");
    }

}

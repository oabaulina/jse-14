package ru.baulina.tm.controller;

import ru.baulina.tm.api.controller.ITaskController;
import ru.baulina.tm.api.service.IAuthService;
import ru.baulina.tm.api.service.ITaskService;
import ru.baulina.tm.Entity.Task;
import ru.baulina.tm.util.TerminalUtil;

import java.util.List;

public class TaskController implements ITaskController {

    private final ITaskService taskService;
    private final IAuthService authService;

    public TaskController(final ITaskService taskService, final IAuthService authService) {
        this.taskService = taskService;
        this.authService = authService;
    }

    @Override
    public void showTasks() {
        System.out.println("[LIST TASKS]");
        final Long userId = authService.getUserId();
        final List<Task> tasks = taskService.findAll(userId);
        int index = 1;
        for (Task task: tasks) {
            System.out.println(index + ". " + task);
            index++;
        }
        System.out.println("[OK]");
        System.out.println();
    }

    @Override
    public void clearTasks() {
        System.out.println("[CLEAR TASK]");
        final Long userId = authService.getUserId();
        taskService.clear(userId);
        System.out.println("[OK]");
        System.out.println();
    }

    @Override
    public void createTasks() {
        System.out.println("[CREATE TASK]");
        System.out.println("ENTER NAME:");
        final  String name = TerminalUtil.nextLine();
        System.out.println("ENTER DESCRIPTION:");
        final  String description = TerminalUtil.nextLine();
        final Long userId = authService.getUserId();
        taskService.create(userId, name, description);
        System.out.println("[OK]");
        System.out.println();
    }

    @Override
    public void showTaskById() {
        System.out.println("[SHOW TASK]");
        System.out.println("ENTER ID");
        final Long id = TerminalUtil.nexLong();
        final Long userId = authService.getUserId();
        final Task task = taskService.findOneById(userId, id);
        if (task == null) {
            System.out.println("[FAIL]");
            return;
        }
        showTask(task);
        System.out.println("[OK]");
    }

    @Override
    public void showTaskByIndex() {
        System.out.println("[SHOW TASK]");
        System.out.println("ENTER INDEX");
        final Integer index = TerminalUtil.nexInt() -1;
        final Long userId = authService.getUserId();
        final Task task = taskService.findOneByIndex(userId, index);
        if (task == null) {
            System.out.println("[FAIL]");
            return;
        }
        showTask(task);
        System.out.println("[OK]");
    }

    private  void showTask(final Task task) {
        if (task == null) return;
        System.out.println("ID: " + task.getId());
        System.out.println("NAME: " + task.getName());
        System.out.println("DESCRIPTION: " + task.getDescription());
    }

    @Override
    public void showTaskByName() {
        System.out.println("[SHOW TASK]");
        System.out.println("ENTER NAME");
        final String name = TerminalUtil.nextLine();
        final Long userId = authService.getUserId();
        final Task task = taskService.findOneByName(userId, name);
        if (task == null) {
            System.out.println("[FAIL]");
            return;
        }
        showTask(task);
        System.out.println("[OK]");
    }

    @Override
    public void updateTaskById() {
        System.out.println("[SHOW TASK]");
        System.out.println("ENTER ID:");
        final Long id  = TerminalUtil.nexLong();
        final Long userId = authService.getUserId();
        final Task task = taskService.findOneById(userId, id);
        if (task == null) {
            System.out.println("[FAIL]");
            return;
        }
        System.out.println("ENTER NAME:");
        final String name  = TerminalUtil.nextLine();
        System.out.println("ENTER DESCRIPTION:");
        final String description  = TerminalUtil.nextLine();
        final Task taskUpdated = taskService.updateTaskById(userId, id, name, description);
        if (taskUpdated == null) {
            System.out.println("[FAIL]");
            return;
        }
        System.out.println("[OK]");
    }

    @Override
    public void updateTaskByIndex() {
        System.out.println("[SHOW TASK]");
        System.out.println("ENTER INDEX:");
        final Integer index  = TerminalUtil.nexInt() -1;
        final Long userId = authService.getUserId();
        final Task task = taskService.findOneByIndex(userId, index);
        if (task == null) {
            System.out.println("[FAIL]");
            return;
        }
        System.out.println("ENTER NAME:");
        final String name  = TerminalUtil.nextLine();
        System.out.println("ENTER DESCRIPTION:");
        final String description  = TerminalUtil.nextLine();
        final Task taskUpdated = taskService.updateTaskByIndex(userId, index, name, description);
        if (taskUpdated == null) {
            System.out.println("[FAIL]");
            return;
        }
        System.out.println("[OK]");
    }

    @Override
    public void removeTaskById() {
        System.out.println("[SHOW TASK]");
        System.out.println("ENTER ID");
        final Long id = TerminalUtil.nexLong();
        final Long userId = authService.getUserId();
        final Task task = taskService.removeOneById(userId, id);
        if (task == null) System.out.println("[FAIL]");
        else System.out.println("[OK]");
    }

    @Override
    public void removeTaskByIndex() {
        System.out.println("[SHOW TASK]");
        System.out.println("ENTER INDEX");
        final Integer index = TerminalUtil.nexInt() -1;
        final Long userId = authService.getUserId();
        final Task task = taskService.removeOneByIndex(userId, index);
        if (task == null) System.out.println("[FAIL]");
        else System.out.println("[OK]");
    }

    @Override
    public void removeTaskByName() {
        System.out.println("[SHOW TASK]");
        System.out.println("ENTER NAME");
        final String name = TerminalUtil.nextLine();
        final Long userId = authService.getUserId();
        final Task task = taskService.removeOneByName(userId, name);
        if (task == null) System.out.println("[FAIL]");
        else System.out.println("[OK]");
    }

}

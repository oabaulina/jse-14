package ru.baulina.tm.bootstrap;

import ru.baulina.tm.api.controller.IAuthController;
import ru.baulina.tm.api.controller.ICommandController;
import ru.baulina.tm.api.controller.IProjectController;
import ru.baulina.tm.api.controller.ITaskController;
import ru.baulina.tm.api.repository.ICommandRepository;
import ru.baulina.tm.api.repository.IProjectRepository;
import ru.baulina.tm.api.repository.ITaskRepository;
import ru.baulina.tm.api.repository.IUserRepository;
import ru.baulina.tm.api.service.*;
import ru.baulina.tm.constant.ArgumentConst;
import ru.baulina.tm.constant.CommandConst;
import ru.baulina.tm.controller.AuthController;
import ru.baulina.tm.controller.CommandController;
import ru.baulina.tm.controller.ProjectController;
import ru.baulina.tm.controller.TaskController;
import ru.baulina.tm.exeption.system.EmptyArgumentException;
import ru.baulina.tm.exeption.system.IncorrectArgumentException;
import ru.baulina.tm.exeption.system.EmptyCommandException;
import ru.baulina.tm.exeption.system.IncorrectCommandException;
import ru.baulina.tm.repository.CommandRepository;
import ru.baulina.tm.repository.ProjectRepository;
import ru.baulina.tm.repository.TaskRepository;
import ru.baulina.tm.repository.UserRepository;
import ru.baulina.tm.role.Role;
import ru.baulina.tm.service.*;
import ru.baulina.tm.util.TerminalUtil;

public final class Bootstrap {

    private final IUserRepository userRepository = new UserRepository();

    private final IUserService userService = new UserService(userRepository);

    private final IAuthService authService = new AuthService(userService);

    private final IAuthController authController = new AuthController(authService);

    private final ICommandRepository commandRepository = new CommandRepository();

    private final ICommandService commandService = new CommandService(commandRepository);

    private final ICommandController commandController = new CommandController(commandService);

    private final ITaskRepository taskRepository = new TaskRepository();

    private final ITaskService taskService = new TaskService(taskRepository);

    private final ITaskController taskController = new TaskController(taskService, authService);

    private final IProjectRepository projectRepository = new ProjectRepository();

    private final IProjectService projectService = new ProjectService(projectRepository);

    private final IProjectController projectController = new ProjectController(projectService, authService);

    private void initUsers()  {
        userService.create("test", "test", "test@test.ru");
        userService.create("admin", "admin", Role.ADMIN);
    }

    public void run(final  String[] args) {
        commandController.displayWelcome();
        if (parseCommands(args) || parseArgs(args)) commandController.exit();
        process();
    }

    private void process() {
        initUsers();
        while (true) {
            try {
                parseCommand(TerminalUtil.nextLine());
            } catch (Exception e) {
                System.err.println(e.getMessage());
                System.err.println("[FAIL]");
            }
        }
    }

    private boolean parseArgs(final String[] args) {
        if (args == null || args.length == 0) return false;
        final String arg = args[0];
        return parseArg(arg);
    }

    private boolean parseArg(final String arg) {
        if (arg == null || arg.isEmpty()) throw new EmptyArgumentException();
        switch (arg) {
            case ArgumentConst.HELP: commandController.showHelp(); return true;
            case ArgumentConst.ABOUT: commandController.showAbout(); return true;
            case ArgumentConst.VERSION: commandController.showVersion(); return true;
            case ArgumentConst.COMMANDS: commandController.showCommands(); return true;
            case ArgumentConst.ARGUMENTS: commandController.showArguments(); return true;
            case ArgumentConst.INFO: commandController. showInfo(); return true;
            default: throw new IncorrectArgumentException();
        }
    }

    private boolean parseCommands(final String[] commands) {
        if (commands == null || commands.length == 0) return false;
        final String command = commands[0];
        return parseCommand(command);
    }

    private boolean parseCommand(final String command) {
        if (command == null || command.isEmpty()) throw new EmptyCommandException();
        switch (command.toLowerCase()) {
            case CommandConst.HELP: commandController.showHelp(); return true;
            case CommandConst.ABOUT: commandController.showAbout(); return true;
            case CommandConst.VERSION: commandController.showVersion(); return true;
            case CommandConst.COMMANDS: commandController.showCommands(); return true;
            case CommandConst.ARGUMENTS: commandController.showArguments(); return true;
            case CommandConst.INFO: commandController.showInfo(); return true;
            case CommandConst.EXIT: commandController.exit(); return true;

            case CommandConst.TASK_CREATE: taskController.createTasks(); return true;
            case CommandConst.TASK_LIST: taskController.showTasks(); return true;
            case CommandConst.TASK_CLEAR: taskController.clearTasks(); return true;
            case CommandConst.TASK_UPDATE_BY_ID: taskController.updateTaskById(); return true;
            case CommandConst.TASK_UPDATE_BY_INDEX: taskController.updateTaskByIndex(); return true;
            case CommandConst.TASK_VIEW_BY_ID: taskController.showTaskById(); return true;
            case CommandConst.TASK_VIEW_BY_INDEX: taskController.showTaskByIndex(); return true;
            case CommandConst.TASK_VIEW_BY_NAME: taskController.showTaskByName(); return true;
            case CommandConst.TASK_REMOVE_BY_ID: taskController.removeTaskById(); return true;
            case CommandConst.TASK_REMOVE_BY_INDEX: taskController.removeTaskByIndex(); return true;
            case CommandConst.TASK_REMOVE_BY_NAME: taskController.removeTaskByName(); return true;
            case CommandConst.PROJECT_UPDATE_BY_ID: projectController.updateProjectById(); return true;
            case CommandConst.PROJECT_UPDATE_BY_INDEX: projectController.updateProjectByIndex(); return true;
            case CommandConst.PROJECT_VIEW_BY_ID: projectController.showProjectById(); return true;
            case CommandConst.PROJECT_VIEW_BY_INDEX: projectController.showProjectByIndex(); return true;
            case CommandConst.PROJECT_VIEW_BY_NAME: projectController.showProjectByName(); return true;
            case CommandConst.PROJECT_REMOVE_BY_ID: projectController.removeProjectById(); return true;
            case CommandConst.PROJECT_REMOVE_BY_INDEX: projectController.removeProjectByIndex(); return true;
            case CommandConst.PROJECT_REMOVE_BY_NAME: projectController.removeProjectByName(); return true;
            case CommandConst.PROJECT_CREATE: projectController.createProjects(); return true;
            case CommandConst.PROJECT_LIST: projectController.showProjects(); return true;
            case CommandConst.PROJECT_CLEAR: projectController.clearProjects(); return true;

            case CommandConst.LOGIN: authController.login(); return true;
            case CommandConst.LOGOUT: authController.logout(); return true;
            case CommandConst.REGISTRY: authController.registry(); return true;
            case CommandConst.LIST_USERS: authController.showListUser(); return true;
            case CommandConst.CHANGE_PASSWORD: authController.changePassword(); return true;
            case CommandConst.PROFILE_OF_USER: authController.profileUser(); return true;
            case CommandConst.CHANGE_PROFILE_OF_USER: authController.changeProfileUser(); return true;
            default: throw new IncorrectCommandException();
        }
    }

}

package ru.baulina.tm.constant;

public class CommandDescriptionConst {

    public static final String HELP = "Display list of terminal commands.";

    public static final String VERSION = "Display program version.";

    public static final String ABOUT = "Display developer info.";

    public static final String EXIT = "Close application.";

    public static final String INFO = "Display information about system.";

    public static final String ARGUMENTS = "Show program arguments.";

    public static final String COMMANDS = "Show program commands.";


    public static final String TASK_CREATE = "Create new task.";

    public static final String TASK_LIST = "Show task list.";

    public static final String TASK_CLEAR = "Remove all tasks.";


    public static final String TASK_UPDATE_BY_ID = "Update task by id.";

    public static final String TASK_UPDATE_BY_INDEX = "Update task by index.";

    public static final String TASK_VIEW_BY_ID = "Show task by id.";

    public static final String TASK_VIEW_BY_INDEX = "Show task by index.";

    public static final String TASK_VIEW_BY_NAME = "Show task by name.";

    public static final String TASK_REMOVE_BY_ID = "Remove task by id.";

    public static final String TASK_REMOVE_BY_INDEX = "Remove task by index.";

    public static final String TASK_REMOVE_BY_NAME = "Remove task by name.";


    public static final String PROJECT_CREATE = "Create new project.";

    public static final String PROJECT_LIST = "Show task project.";

    public static final String PROJECT_CLEAR = "Remove all projects.";


    public static final String PROJECT_UPDATE_BY_ID = "Update project by id.";

    public static final String PROJECT_UPDATE_BY_INDEX = "Update project by index.";

    public static final String PROJECT_VIEW_BY_ID = "Show project by id.";

    public static final String PROJECT_VIEW_BY_INDEX = "Show project by index.";

    public static final String PROJECT_VIEW_BY_NAME = "Show project by name.";

    public static final String PROJECT_REMOVE_BY_ID = "Remove project by id.";

    public static final String PROJECT_REMOVE_BY_INDEX = "Remove project by index.";

    public static final String PROJECT_REMOVE_BY_NAME = "Remove project by name.";


    public static final String LOGIN = "Sign in.";

    public static final String LOGOUT = "Log out.";

    public static final String REGISTRY = "Register new users.";

    public static final String LIST_USERS = "Show users.";

    public static final String CHANGE_PASSWORD = "Change user's password.";

    public static final String PROFILE_OF_USER = "Show user's profiler.";

    public static final String CHANGE_PROFILE_OF_USER = "Change user's profiler.";

}

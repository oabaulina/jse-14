package ru.baulina.tm.Entity;

import java.util.Random;

public abstract class AbstractEntity {

    private long id = Math.abs(new Random().nextLong());

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

}

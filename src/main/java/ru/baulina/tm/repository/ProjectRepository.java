package ru.baulina.tm.repository;

import ru.baulina.tm.Entity.Task;
import ru.baulina.tm.api.repository.IProjectRepository;
import ru.baulina.tm.Entity.Project;

import java.util.ArrayList;
import java.util.List;

public class ProjectRepository implements IProjectRepository {

    private List<Project> projects = new ArrayList<>();

    @Override
    public void add(final Long userId, final Project project) {
        project.setUserId(userId);
        projects.add(project);
    }

    @Override
    public void remove(final Long userId, final Project project) {
        if (userId.equals(project.getUserId())) projects.remove(project);
    }

    @Override
    public List<Project> findAll(final Long userId) {
        final List<Project> result = new ArrayList<>();
        for (final Project project: projects) {
            if (userId.equals(project.getUserId())) result.add(project);
        }
        return result;
    }

    @Override
    public void clear(final Long userId) {
        final List<Project> result = new ArrayList<>();
        for (final Project project: projects) {
            if (userId.equals(project.getUserId())) result.add(project);
        }
        result.clear();
     }

    @Override
    public Project findOneById(final Long userId, final Long id) {
        for (final Project project: projects) {
            if (userId.equals(project.getUserId()) && id.equals(project.getId())) return project;
        }
        return null;
    }

    @Override
    public Project findOneByIndex(final Long userId, final Integer index) {
        final List<Project> result = new ArrayList<>();
        for (final Project project: projects) {
            if (userId.equals(project.getUserId())) result.add(project);
        }
        return result.get(index);
    }

    @Override
    public Project findOneByName(final Long userId, final String name) {
        for (final Project project: projects) {
            if (userId.equals(project.getUserId()) && name.equals(project.getName())) return project;
        }
        return null;
    }

    @Override
    public Project removeOneById(final Long userId, final Long id) {
        final Project project = findOneById(userId, id);
        if (project == null) return null;
        projects.remove(project);
        return project;
    }

    @Override
    public Project removeOneByIndex(final Long userId, final Integer index) {
        final Project project = findOneByIndex(userId, index);
        if (project == null) return null ;
        remove(userId, project);
        return project;
    }

    @Override
    public Project removeOneByName(final Long userId, final String name) {
        final Project project = findOneByName(userId, name);
        if (project == null) return null;
        remove(userId, project);
        return project;
    }

}

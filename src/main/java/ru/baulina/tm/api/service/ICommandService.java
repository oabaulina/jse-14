package ru.baulina.tm.api.service;

import ru.baulina.tm.dto.Command;

public interface ICommandService {

    Command[] getTerminalCommands();

    String[] getCommands();

    String[] getArgs();

}

package ru.baulina.tm.api.service;

import ru.baulina.tm.Entity.Task;

import java.util.List;

public interface ITaskService {

    void create(Long userId, String name);

    void create(Long userId, String name, String description);

    void add(Long userId, Task task);

    void remove(Long userId, Task task);

    List<Task> findAll(Long userId);

    void clear(Long userId);

    Task findOneById(Long userId, Long id);

    Task findOneByIndex(Long userId, Integer index);

    Task findOneByName(Long userId, String name);

    Task removeOneById(Long userId, Long id);

    Task removeOneByIndex(Long userId, Integer index);

    Task removeOneByName(Long userId, String name);

    Task updateTaskById(Long userId, Long id, String name, String  description);

    Task updateTaskByIndex(Long userId, Integer index, String name, String  description);

}

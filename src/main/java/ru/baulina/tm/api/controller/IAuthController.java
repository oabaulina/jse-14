package ru.baulina.tm.api.controller;

public interface IAuthController {

    void login();

    void logout();

    void registry();

    void changePassword();

    void profileUser();

    void showListUser();

    void changeProfileUser();

}

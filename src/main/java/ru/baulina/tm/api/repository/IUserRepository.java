package ru.baulina.tm.api.repository;

import ru.baulina.tm.Entity.User;

import java.util.List;

public interface IUserRepository {

    List<User> findAll();

    User add(User user);

    User findById(Long id);

    User findByLogin(String login);

    User removeUser(User user);

    User removeById(Long id);

    User removeByLogin(String login);

}

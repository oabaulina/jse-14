package ru.baulina.tm.exeption.system;

public class IncorrectArgumentException extends RuntimeException {

    public IncorrectArgumentException() {
        super("Error! Argument not exist...");
    }

}

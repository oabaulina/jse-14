package ru.baulina.tm.exeption.system;

public class IncorrectCommandException extends RuntimeException{

    public IncorrectCommandException() {
        super("Error! Command not exist...");
    }

}

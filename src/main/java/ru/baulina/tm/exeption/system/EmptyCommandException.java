package ru.baulina.tm.exeption.system;

public class EmptyCommandException extends RuntimeException{

    public EmptyCommandException() {
        super("Error! Command is empty...");
    }

}

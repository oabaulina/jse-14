package ru.baulina.tm.exeption.system;

public class EmptyArgumentException extends RuntimeException {

    public EmptyArgumentException() {
        super("Error! Argument is empty...");
    }

}

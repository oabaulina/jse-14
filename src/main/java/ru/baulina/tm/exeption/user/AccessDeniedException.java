package ru.baulina.tm.exeption.user;

public class AccessDeniedException extends RuntimeException {

    public AccessDeniedException() {
        super("Error! Access denied...");
    }

}

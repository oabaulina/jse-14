package ru.baulina.tm.exeption.empty;

public class EmptyIdException extends RuntimeException{

    public EmptyIdException() {
        super("Error! Id is empty...");
    }

}

package ru.baulina.tm.exeption.empty;

public class EmptyEmailException extends RuntimeException{

    public EmptyEmailException() {
        super("Error! Email is empty...");
    }

}

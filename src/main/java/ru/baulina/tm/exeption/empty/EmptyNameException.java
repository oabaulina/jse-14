package ru.baulina.tm.exeption.empty;

public class EmptyNameException extends RuntimeException{

    public EmptyNameException() {
        super("Error! Name is empty...");
    }

}

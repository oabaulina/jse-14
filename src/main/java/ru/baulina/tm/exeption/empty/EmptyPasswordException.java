package ru.baulina.tm.exeption.empty;

public class EmptyPasswordException extends RuntimeException{

    public EmptyPasswordException() {
        super("Error! Password is empty...");
    }

}

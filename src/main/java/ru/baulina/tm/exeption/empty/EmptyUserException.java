package ru.baulina.tm.exeption.empty;

public class EmptyUserException extends RuntimeException{

    public EmptyUserException() {
        super("Error! User Id is empty...");
    }
}

package ru.baulina.tm.exeption.empty;

public class EmptyRoleException extends RuntimeException{

    public EmptyRoleException() {
        super("Error! Role is empty...");
    }

}

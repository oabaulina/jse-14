package ru.baulina.tm.exeption.empty;

public class EmptyLoginException extends RuntimeException{

    public EmptyLoginException() {
        super("Error! Login is empty...");
    }

}
